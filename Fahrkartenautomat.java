import java.util.Scanner;

class Fahrkartenautomat {
    static int fuenfcentmuenze = 10;
    static int zweieuromuenze = 10;
    static int eineuromuenze = 10;
    static int fuenfzicentmuenze = 10;
    static int zehncentmuenze = 10;
    static int zwanzigcentmuenze = 10;
    static int ergfahrkartenanzahl = 0;
    static double zwischensumme = 0;
    static int rohlinge = 10;
    static int a = 0;

    public static void main(String[] args) { // Main
        fahrkartenBestellung();
    }

    static void fahrkartenBestellung() { // Ticket Auswahl
        double[] kosteneinzel = { 2.9, 3.3, 3.6, 1.9 }; // Einzelfahrschein Kosten
        double[] kostentag = { 8.6, 9, 9.6 }; // Tageskarten Kosten
        double[] kostengruppe = { 23.5, 24.3, 24.9 }; // Kleingruppen-Tageskarten Kosten

        Scanner ticket = new Scanner(System.in); // Scanner für erst Auswahl
        printfahrkartenauswahl(); // Optische Auswahl
        System.out.println();
        System.out.print("Ihre Auswahl:"); // Auswahl Einzelfahrschein, Tageskarte oder Kleingruppen-Tageskarte
        int ticketauswahl = ticket.nextInt();
        System.out.println();

        switch (ticketauswahl) { // Optische Auswahl
            case 0:
                printeinzelfahrausweise(); // Einzelfahrausweise
                break;
            case 1:
                printtageskarten(); // Tageskarten
                break;
            case 2:
                printkleingruppentageskarten(); // Kleingruppen-Tageskarten
                break;
            case 9:
                fahrkartenBezahlen(); // Weiterleiten zur Bezahlung
                a = 1; // Um vorgang Abzubrechen nach Bezahlung
                break;
            case 999:
                admin();
                return;
            default:
                System.out.println("Ein Fehler ist Aufgetreten"); // Wenn eine Ungültige Eingabe erfolgt
                fahrkartenBestellung(); // Aufrufen Ticketauswahl um erneute Auwahl zu tätigen
                break;
        }
        if (a == 0) { // Vorgang wird Abgebrochen wenn a = 1, wenn Bezahlung getätigt wurde
            Scanner weitereauswahl = new Scanner(System.in); // Auswahl in den Unterkategorien
            System.out.println();
            System.out.print("Ihre Auswahl: ");
            int ticketauswahl2 = weitereauswahl.nextInt();

            if (ticketauswahl2 > 3) {
                System.out.println("Fehlerhate Eingabe");
                fahrkartenBestellung();
            }

            if (ticketauswahl2 == 9) { // Zurück Funktion
                fahrkartenBestellung(); // Ticketauswahl
            }

            Scanner eingabe = new Scanner(System.in); // Anzahl der Tickets
            System.out.print("Anzahl der Fahrkarten Bitte: ");
            int fahrkartenanzahl = eingabe.nextInt();
            if (fahrkartenanzahl <= 0 || fahrkartenanzahl > 10) { // Anzahl muss zwischen 1 und 10 sein
                System.out.println("Ungültige Ticket anzahl"); // Wenn nicht, wird eine Fehlermeldung ausgegeben
                System.out.println("Die Ticket Anzahl wird auf 1 gesetzt");
                fahrkartenanzahl = 1; // Ticket Anzahl wird auf 1 gesetzt bei fehlerhaften eingabe
            }
            if (ticketauswahl == 0) { // Einzelfahrausweise
                zwischensumme = zwischensumme + (fahrkartenanzahl * kosteneinzel[ticketauswahl2]);// Kosten werden
                                                                                                  // berrechnet
                ergfahrkartenanzahl += fahrkartenanzahl; // Fahrkartenanzahl wird erhöht
                zwischensumme(); // Zwischensumme wird ausgerechnet
                fahrkartenBestellung(); // Fahrkarten Auswahl wird aufgerufen
            } else if (ticketauswahl == 1) { // Tageskarten
                zwischensumme = zwischensumme + (fahrkartenanzahl * kostentag[ticketauswahl2]);
                ergfahrkartenanzahl += fahrkartenanzahl;
                zwischensumme();
                fahrkartenBestellung();
            } else if (ticketauswahl == 2) { // Kleingruppen-Tageskarten
                ergfahrkartenanzahl += fahrkartenanzahl;
                zwischensumme = zwischensumme + (fahrkartenanzahl * kostengruppe[ticketauswahl2]);
                zwischensumme();
                fahrkartenBestellung();
            }
        }
    }

    static void zwischensumme() { // Zwischensumme bilden
        System.out.println();
        System.out.print("Die Zwischensumme beträgt ");
        System.out.printf("|%02.2f|", zwischensumme);
        System.out.print("Euro");
        System.out.println();
    }

    static void fahrkartenBezahlen() { // Bezahlvorgang
        rohlinge = rohlinge - ergfahrkartenanzahl;
        if (rohlinge <= 0) {
            System.out.println("Es sind nicht genügend Rohlinge mehr vorhanden, wir bitten um Entschuldigung.");
            fahrkartenBestellung();
        } else {
            double zubezahlen = zwischensumme;
            double bezahlt = 0;
            double rueckzahlung = 0;
            System.out.printf("Zu Bezahlen sind");
            System.out.printf("|%02.2f|", zubezahlen);
            System.out.print("Euro");
            System.out.println(" Minimal 5ct., Maximal 2 EURO");
            while (bezahlt <= zubezahlen) { // Vorgang wird Abgebrochen wenn mehr oder gleich viel Bezahlt wurde
                System.out.println("-------------------------------------------------------");
                System.out.print("Bitte werfen sie jetzt ihr Geld ein: ");
                Scanner geldeinwurf = new Scanner(System.in); // Scanner für Geldeinwurf
                double geld = geldeinwurf.nextDouble();
                System.out.println("-------------------------------------------------------");
                if (geld == 0.05) { // 5ct.
                    bezahlt = bezahlt + geld;
                    fuenfcentmuenze++;
                } else if (geld == 0.1) { // 10ct.
                    bezahlt = bezahlt + geld;
                    zehncentmuenze++;
                } else if (geld == 0.2) { // 20ct.
                    bezahlt = bezahlt + geld;
                    zwanzigcentmuenze++;
                } else if (geld == 0.5) { // 50ct.
                    bezahlt = bezahlt + geld;
                    fuenfzicentmuenze++;
                } else if (geld == 1) { // 1€
                    bezahlt = bezahlt + geld;
                    eineuromuenze++;
                } else if (geld == 2) { // 2€
                    bezahlt = bezahlt + geld;
                    zweieuromuenze++;
                } else { // Wenn der Geldeinwurf nicht mit den oberen Zahlen übereinstimmt kommt eine
                         // Fehlermeldung
                    System.out.println("Fehlerhafter Geld einwurf!");
                }
                System.out.print("Es sind bereits ");
                System.out.printf("|%02.2f|", bezahlt);
                System.out.print("Euro von");
                System.out.printf("|%02.2f|", zubezahlen);
                System.out.print("Euro gezahlt");
                System.out.println();
            }
            rueckzahlung = bezahlt - zubezahlen;
            rueckgeldAusgeben(rueckzahlung); // Geld rückgabe Funktion
        }
    }

    static void warten() { // Warten zwischen "=" für den Ladebalken
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
    }

    static void fahrkartenAusgeben() { // Ticket Ausgabe
        // Vor.: Eine gültige Fahrkartenanzahl wurde eingegeben.
        // Eff.: /
        // Erg.: Die Fahrkartenanzahl die entnommen werden kann ist geliefert.
        System.out.println();
        System.out.println("Druckvorgang ***intense Drucker Geräusche***");
        System.out.println("0%.................................................100%"); // Ladeblaken/ Druckfortschritt
        for (int i = 0; i < 55; i++) {
            System.out.print("=");
            warten();
        }
        System.out.println();
        System.out.println("Bitte entnehemen sie ihr(e) " + ergfahrkartenanzahl + " Ticket(s)");
    }

    static void rueckgeldAusgeben(double rueckzahlung) { // Geldrückgabe
        // Vor.: Eine Erfolgreiche Fahrkartenbezahlung wurde abgeschlossen.
        // Eff.: /
        // Erg.: Die Rückzahlung ist geliefert.
        System.out.println();
        int zweieuro = 0;
        int eineuro = 0;
        int fünfzigcent = 0;
        int zwanzigcent = 0;
        int zehncent = 0;
        int fünfcent = 0;
        rueckzahlung = rueckzahlung * 100;
        int rueck = (int) rueckzahlung;
        int gesammt = 0;
        while (rueck > 0) {
            if (rueck >= 200) {
                rueckzahlung = rueck - 200;
                zweieuro++;
                gesammt++;
                zweieuromuenze--;
            } else if (rueck >= 100) {
                rueck = rueck - 100;
                eineuro++;
                gesammt++;
                eineuromuenze--;
            } else if (rueck >= 50) {
                rueck = rueck - 50;
                fünfzigcent++;
                gesammt++;
                fuenfzicentmuenze--;
            } else if (rueck >= 20) {
                rueck = rueck - 20;
                zwanzigcent++;
                gesammt++;
                zwanzigcentmuenze--;
            } else if (rueck >= 10) {
                rueck = rueck - 10;
                zehncent++;
                gesammt++;
                zehncentmuenze--;
            } else if (rueck >= 5) {
                rueck = rueck - 5;
                fünfcent++;
                gesammt++;
                fuenfcentmuenze--;
            } else {
                break;
            }
        }
        if (zweieuro > zweieuromuenze || eineuro > eineuromuenze || fünfzigcent > fuenfzicentmuenze
                || zwanzigcent > zwanzigcentmuenze || zehncent > zehncentmuenze || fünfcent > fuenfcentmuenze) {
            System.out
                    .println("Kein passendes Wechselgeld vorhanden, bitte versuchen sie es mit anderen Münzen erneut");
        } else {
            fahrkartenAusgeben();
            rueckzahlung = rueckzahlung / 100;
            System.out.println();
            System.out.print("Bitte das Rückgeld in höhe von");
            System.out.printf("|%02.2f|", rueckzahlung);
            System.out.print("Euro entnehemen");
            muenzenausgeben(zehncent, zweieuro, eineuro, fünfcent, fünfzigcent, zwanzigcent, gesammt);
        }
    }

    static void muenzenausgeben(int zehncent, int zweieuro, int eineuro, int fünfcent, int fünfzigcent,
            int zwanzigcent, int gesammt) { // Wie oft eine Münze Ausgegeben wird
        int value = 0;
        String muenz = "Euro";
        System.out.println();

        muenzanzeige1(gesammt);
        while (zweieuro > 0) {
            value = 2;
            muenzanzeige(value, muenz);
            zweieuro--;
        }
        while (eineuro > 0) {
            value = 1;
            muenzanzeige(value, muenz);
            eineuro--;
        }
        while (fünfzigcent > 0) {
            value = 50;
            muenz = "Cent";
            muenzanzeige(value, muenz);
            fünfzigcent--;
        }
        while (zwanzigcent > 0) {
            value = 20;
            muenz = "Cent";
            muenzanzeige(value, muenz);
            zwanzigcent--;
        }
        while (zehncent > 0) {
            value = 10;
            muenz = "Cent";
            muenzanzeige(value, muenz);
            zehncent--;
        }
        while (fünfcent > 0) {
            value = 5;
            muenz = "Cent";
            muenzanzeige(value, muenz);
            fünfcent--;
        }
        muenzanzige2(gesammt);
    }

    static void muenzanzeige1(int gesammt) { // Münzen Header
        int wert = gesammt;
        int wert2 = gesammt;
        for (int g = wert; g > 0; g--) {
            System.out.print("   * * * *          ");
        }
        System.out.println();
        for (int b = wert2; b > 0; b--) {
            System.out.print(" *         *        ");
        }
        System.out.println();
    }

    static void muenzanzeige(int value, String muenz) { // Münzen Mitte, Wert der Münzen
        if (value == 1 || value == 2 || value == 5) {
            System.out.print("*   " + value + muenz + "   *       ");
        }
        if (value == 10 || value == 20 || value == 50) {
            System.out.print("*  " + value + muenz + "   *       ");
        }
    }

    static void muenzanzige2(int gesammt) { // Münzen Footer

        int wert3 = gesammt;
        int wert4 = gesammt;
        int wert5 = gesammt;
        System.out.println();
        for (int e = wert5; e > 0; e--) {
            System.out.print("*           *       ");
        }
        System.out.println();
        for (int c = wert3; c > 0; c--) {
            System.out.print(" *         *        ");
        }
        System.out.println();
        for (int d = wert4; d > 0; d--) {
            System.out.print("   * * * *          ");
        }
        fahrkartenBestellung();
    }

    static void admin() { // Admin Pannel
        Scanner pw = new Scanner(System.in);
        System.out.print("Bitte geben sie das Passwort ein: ");
        String passwort = pw.nextLine();
        if (passwort.equals("passwort")) {
            System.out.println("===========Administrator-Menü===========");
            System.out.println("Kassen stand                         (0)");
            System.out.println("Kasse Leeren                         (1)");
            System.out.println("Kasse Auffüllen                      (2)");
            System.out.println("Rohlinge Auffüllen                   (3)");
            System.out.println("Zurück                               (9)");
            System.out.println("========================================");
            Scanner eingabe = new Scanner(System.in);
            System.out.print("Ihre Auswahl: ");
            int auswahl = eingabe.nextInt();
            switch (auswahl) {
                case 0:
                    geldstand();
                    break;
                case 1:
                    leeren();
                    break;
                case 2:
                    fuellen();
                    break;
                case 3:
                    rohlingefuellen();
                    break;
                case 9:
                    fahrkartenBestellung();
                    break;
                default:
                    System.out.print("Fehlerhafte eingabe, bitte versuchen sie es erneut");
                    admin();
                    break;
            }
        } else {
            System.out.print("Falsches Passwort");
            fahrkartenBestellung();
        }

    }

    static void geldstand() { // Geldstand anzeigen

        System.out.println();
        System.out.println("========================================");
        System.out.println("2 Euro:                               " + zweieuromuenze);
        System.out.println("1 Euro:                               " + eineuromuenze);
        System.out.println("50 Cent:                              " + fuenfzicentmuenze);
        System.out.println("20 Cent:                              " + zwanzigcentmuenze);
        System.out.println("10 Cent:                              " + zehncentmuenze);
        System.out.println("5 Cent:                               " + fuenfcentmuenze);
        System.out.println("Zurück                               (9)");
        System.out.println("========================================");
        Scanner zurueck = new Scanner(System.in);
        System.out.print("Um zurück zu kehren drücken sie die 9: ");
        int zur = zurueck.nextInt();
        switch (zur) {
            case 9:
                fahrkartenBestellung();
                break;
            default:
                System.out.println("Fehlerhafte Eingabe");
                geldstand();
        }
    }

    static void leeren() { // Geldstand Leeren
        System.out.println("============================");
        System.out.println("Alles Leeren             (1)");
        System.out.println("2€ Leeren                (2)");
        System.out.println("1€ Leeren                (3)");
        System.out.println("50 Cent Leeren           (4)");
        System.out.println("20 Cent Leeren           (5)");
        System.out.println("10 Cent Leeren           (6)");
        System.out.println("5 Cent Leeren            (7)");
        System.out.println("Zurück                   (9)");
        System.out.println("============================");
        Scanner leeren = new Scanner(System.in);
        System.out.println("Ihre Auswahl: ");
        int leer = leeren.nextInt();
        switch (leer) {
            case 1:
                zweieuromuenze = 0;
                eineuromuenze = 0;
                fuenfzicentmuenze = 0;
                zwanzigcentmuenze = 0;
                zehncentmuenze = 0;
                fuenfcentmuenze = 0;
                break;
            case 2:
                zweieuromuenze = 0;
                break;
            case 3:
                eineuromuenze = 0;
                break;
            case 4:
                fuenfzicentmuenze = 0;
                break;
            case 5:
                zwanzigcentmuenze = 0;
                break;
            case 6:
                zehncentmuenze = 0;
                break;
            case 7:
                fuenfcentmuenze = 0;
                break;
            case 9:
                admin();
        }
    }

    static void fuellen() { // Geldstandauffüllen
        System.out.println("===============================");
        System.out.println("2€ Auffüllen                (1)");
        System.out.println("1€ Auffüllen                (2)");
        System.out.println("50 Cent Auffüllen           (3)");
        System.out.println("20 Cent Auffüllen           (4)");
        System.out.println("10 Cent Auffüllen           (5)");
        System.out.println("5 Cent Auffüllen            (6)");
        System.out.println("Zurück                      (9)");
        System.out.println("===============================");
        Scanner auswahl = new Scanner(System.in);
        System.out.print("Ihre Auswahl: ");
        int ausw = auswahl.nextInt();
        if (ausw == 9) {
            admin();
        } else {
            Scanner anzahl = new Scanner(System.in);
            System.out.print("Wie viel soll Aufgefüllt werden: ");
            int anz = anzahl.nextInt();
            switch (ausw) {
                case 1:
                    zweieuromuenze += anz;
                    break;
                case 2:
                    eineuromuenze += anz;
                    break;
                case 3:
                    fuenfzicentmuenze += anz;
                    break;
                case 4:
                    zwanzigcentmuenze += anz;
                    break;
                case 5:
                    zehncentmuenze += anz;
                    break;
                case 6:
                    fuenfcentmuenze += anz;
                    break;
                default:
                    System.out.println("Fehlerhafte Eingabe");
            }
        }
        fahrkartenBestellung();
    }

    static void rohlingefuellen() { // Rohlinge Auffüllen
        System.out.println("===========================");
        System.out.println("Auffüllen               (1)");
        System.out.println("Zurück                  (9)");
        System.out.println("===========================");
        Scanner auswahl = new Scanner(System.in);
        System.out.print("Ihre Auswahl: ");
        int ausw = auswahl.nextInt();
        switch (ausw) {
            case 1:
                Scanner anzahl = new Scanner(System.in);
                System.out.print("Wie viel soll Aufgefüllt werden: ");
                int anz = anzahl.nextInt();
                rohlinge += anz;
                fahrkartenBestellung();
                break;
            case 9:
                fahrkartenBestellung();
                break;
            default:
                System.out.println("Fehlerhate Eingabe");
                fahrkartenBestellung();
                break;
        }
    }

    static void printfahrkartenauswahl() { // Auswahlmenü
        System.out.println();
        System.out.println("=======================================================");
        System.out.println("Einzelfahrausweise                                  (0)");
        System.out.println("Tageskarten                                         (1)");
        System.out.println("Kleingruppen-Tageskarten                            (2)");
        System.out.println("Bezahlen                                            (9)");
        System.out.println("=======================================================");
    }

    static void printeinzelfahrausweise() { // Auswahlmenü Einzelfahrausweise
        System.out.println("=======================================================");
        System.out.println("Einzelfahrschein Berlin AB               [2,90 EUR] (0)");
        System.out.println("Einzelfahrschein Berlin BC               [3,30 EUR] (1)");
        System.out.println("Einzelfahrschein Berlin ABC              [3,60 EUR] (2)");
        System.out.println("Kurzstrecke                              [1,90 EUR] (3)");
        System.out.println("Zurück                                              (9)");
        System.out.println("=======================================================");
    }

    static void printtageskarten() { // Auswahlmenü Tageskarten
        System.out.println("=======================================================");
        System.out.println("Tageskarte Berlin AB                     [8,60 EUR] (0)");
        System.out.println("Tageskarte Berlin BC                     [9,00 EUR] (1)");
        System.out.println("Tageskarte Berlin ABC                    [9,60 EUR] (2)");
        System.out.println("Zurück                                              (9)");
        System.out.println("=======================================================");
    }

    static void printkleingruppentageskarten() { // Auswahlmenü Kleingruppen-Tageskarten
        System.out.println("=======================================================");
        System.out.println("Kleingruppen-Tageskarte AB              [23,50 EUR] (0)");
        System.out.println("Kleingruppen-Tageskarte BC              [24,30 EUR] (1)");
        System.out.println("Kleingruppen-Tageskarte ABC             [24,90 EUR] (2)");
        System.out.println("Zurück                                              (9)");
        System.out.println("=======================================================");
    }
}
